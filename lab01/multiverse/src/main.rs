// hmm this doesn't look right!!
struct universe_details {
    pub universe_name: String,
    pub universe_winner: String,
    pub universe_population: u32,
}

fn get_universe_details(universe_id: u32) -> Option<universe_details> {
    // does this even compile??
    if universe_id % 15 == 0 {
        Some(universe_details {
            universe_name: "Stardew Valley".to_string(),
            universe_winner: "Jojo Corp".to_string(),
            universe_population: 1,
        })
    } else if universe_id % 5 == 0 {
        Some(universe_details {
            universe_name: "Miraculous".to_string(),
            universe_winner: "Hawk Moth".to_string(),
            universe_population: 22,
        })
    } else if universe_id % 3 == 0 {
        Some(universe_details {
            universe_name: "Star Wars".to_string(),
            universe_winner: "The Rebellion".to_string(),
            universe_population: 4294967295,
        })
    } else {
        None
    }
}


// this main function is fine, except for two gaps
// the print statements could make use of "{variable}" instead of 
// ("{}", variable)
fn main() {
    for id in 1..=15 {
        let universe_details = get_universe_details(id);
        if let Some(details) = universe_details {
            println!("Universe with id {id} is called {}, won by {} and has a population of {}",
            details.universe_name, details.universe_winner, details.universe_population);
        } else {
            println!("Universe with id {id} is unknown");
        }
    }
}

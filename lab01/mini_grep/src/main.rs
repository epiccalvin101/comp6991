fn main() {
    let pattern_string = std::env::args()
        .nth(1)
        .expect("missing required command-line argument: <pattern>");

    let pattern = &pattern_string;

    // TODO: Replace the following with your code:
    loop {
        let mut line = String::new();
        let _ = std::io::stdin().read_line(&mut line);
        if line.trim().is_empty() {
            break;
        } else if line.contains(pattern) {
            print!("{line}");            
        }
        
        line.clear();
    }
}

use bmp::{Pixel, Image};

fn main() {
    let mut img = Image::new(200, 200);
    
    // background
    for (x, y) in img.coordinates() {
        img.set_pixel(x, y, Pixel {r: 255, g: 255, b: 255,});
    }

    // 1
    for x in 48..96 {
        for y in 0..8 {
            img.set_pixel(x + 32, y + 16, Pixel {r: 0, g: 0, b: 0,});
        }
    }

    // 2
    for x in 40..48 {
        for y in 8..16 {
            img.set_pixel(x + 32, y + 16, Pixel {r: 0, g: 0, b: 0,});
        }
    }
    for x in 96..104 {
        for y in 8..16 {
            img.set_pixel(x + 32, y + 16, Pixel {r: 0, g: 0, b: 0,});
        }
    }
        // rgb(236, 20, 31)
    for x in 48..96 {
        for y in 8..16 {
            img.set_pixel(x + 32, y + 16, Pixel {r: 236, g: 20, b: 31,});
        }
    }

    // 3
    for x in 32..40 {
        for y in 16..24 {
            img.set_pixel(x + 32, y + 16, Pixel {r: 0, g: 0, b: 0,});
        }
    }
    for x in 104..112 {
        for y in 16..24 {
            img.set_pixel(x + 32, y + 16, Pixel {r: 0, g: 0, b: 0,});
        }
    }
        // rgb(236, 20, 31)
    for x in 40..104 {
        for y in 16..24 {
            img.set_pixel(x + 32, y + 16, Pixel {r: 236, g: 20, b: 31,});
        }
    }

    // 4
    for x in 24..32 {
        for y in 24..160 {
            img.set_pixel(x + 32, y + 16, Pixel {r: 0, g: 0, b: 0,});
        }
    }
    for x in 64..112 {
        for y in 24..32 {
            img.set_pixel(x + 32, y + 16, Pixel {r: 0, g: 0, b: 0,});
        }
    }
        // rgb(236, 20, 31)
    for x in 32..64 {
        for y in 24..32 {
            img.set_pixel(x + 32, y + 16, Pixel {r: 236, g: 20, b: 31,});
        }
    }

    // 5
    for y in 32..40 {
        for x in 56..64 {
            img.set_pixel(x + 32, y + 16, Pixel {r: 0, g: 0, b: 0,});
        }
        for x in 112..120 {
            img.set_pixel(x + 32, y + 16, Pixel {r: 0, g: 0, b: 0,});
        }
    }
        // rgb(236, 20, 31)
    for x in 32..56 {
        for y in 32..40 {
            img.set_pixel(x + 32, y + 16, Pixel {r: 236, g: 20, b: 31,});
        }
    }

    // 6
    for x in 48..56 {
        for y in 40..64 {
            img.set_pixel(x + 32, y + 16, Pixel {r: 0, g: 0, b: 0,});
        }
    }
    for x in 120..128 {
        for y in 40..64 {
            img.set_pixel(x + 32, y + 16, Pixel {r: 0, g: 0, b: 0,});
        }
    }
        // rgb(236, 20, 31)
    for x in 32..48 {
        for y in 40..48 {
            img.set_pixel(x + 32, y + 16, Pixel {r: 236, g: 20, b: 31,});
        }
    }

    // 7
    for x in 8..24 {
        for y in 48..56 {
            img.set_pixel(x + 32, y + 16, Pixel {r: 0, g: 0, b: 0,});
        }
    }
        // rgb(236, 20, 31)
    for x in 40..48 {
        for y in 48..88 {
            img.set_pixel(x + 32, y + 16, Pixel {r: 236, g: 20, b: 31,});
        }
    }

    // 8
    for x in 0..8 {
        for y in 56..128 {
            img.set_pixel(x + 32, y + 16, Pixel {r: 0, g: 0, b: 0,});
        }
    }
        // rgb(236, 20, 31)
    for x in 8..24 {
        for y in 56..72 {
            img.set_pixel(x + 32, y + 16, Pixel {r: 236, g: 20, b: 31,});
        }
    }

    // 9
    for y in 64..72 {
        for x in 56..64 {
            img.set_pixel(x + 32, y + 16, Pixel {r: 0, g: 0, b: 0,});
        }    
    }
    for x in 112..120 {
        for y in 64..160 {
            img.set_pixel(x + 32, y + 16, Pixel {r: 0, g: 0, b: 0,});
        }
    }
        // rgb(236, 20, 31)
    for x in 48..56 {
        for y in 64..104 {
            img.set_pixel(x + 32, y + 16, Pixel {r: 236, g: 20, b: 31,});
        }
    }

    // 10
    for x in 64..112 {
        for y in 72..80 {
            img.set_pixel(x + 32, y + 16, Pixel {r: 0, g: 0, b: 0,});
        }
    }
        // rgb(236, 20, 31)
    for x in 8..16 {
        for y in 72..80 {
            img.set_pixel(x + 32, y + 16, Pixel {r: 236, g: 20, b: 31,});
        }
    }
    for x in 56..64 {
        for y in 72..120 {
            img.set_pixel(x + 32, y + 16, Pixel {r: 236, g: 20, b: 31,});
        }
    }

    // 11
        // rgb(236, 20, 31)
    for x in 64..96 {
        for y in 80..120 {
            img.set_pixel(x + 32, y + 16, Pixel {r: 236, g: 20, b: 31,});
        }
    }
    for x in 96..112 {
        for y in 80..96 {
            img.set_pixel(x + 32, y + 16, Pixel {r: 236, g: 20, b: 31,});
        }
    }

    // 13
        // rgb(236, 20, 31)
    for x in 96..104 {
        for y in 96..112 {
            img.set_pixel(x + 32, y + 16, Pixel {r: 236, g: 20, b: 31,});
        }
    }

    // 17
    for x in 8..24 {
        for y in 128..136 {
            img.set_pixel(x + 32, y + 16, Pixel {r: 0, g: 0, b: 0,});
        }
    }
        

    // 18
    for x in 56..88 {
        for y in 136..144 {
            img.set_pixel(x + 32, y + 16, Pixel {r: 0, g: 0, b: 0,});
        }
    }

    // 19
    for y in 144..160 {
        for x in 56..64 {
            img.set_pixel(x + 32, y + 16, Pixel {r: 0, g: 0, b: 0,});
        }
        for x in 80..88 {
            img.set_pixel(x + 32, y + 16, Pixel {r: 0, g: 0, b: 0,});
        }
    }


    // 21
    for y in 160..168 {
        for x in 32..56 {
            img.set_pixel(x + 32, y + 16, Pixel {r: 0, g: 0, b: 0,});
        }
        for x in 88..112 {
            img.set_pixel(x + 32, y + 16, Pixel {r: 0, g: 0, b: 0,});
        }
    }

    // rgb(128, 6, 45)
    for x in 16..24 {
        for y in 72..80 {
            img.set_pixel(x + 32, y + 16, Pixel {r: 128, g: 6, b: 45,});
        }
    }
    for x in 8..24 {
        for y in 80..128 {
            img.set_pixel(x + 32, y + 16, Pixel {r: 128, g: 6, b: 45,});
        }
    }
    for x in 32..40 {
        for y in 48..160 {
            img.set_pixel(x + 32, y + 16, Pixel {r: 128, g: 6, b: 45,});
        }
    }
    for x in 40..48 {
        for y in 88..160 {
            img.set_pixel(x + 32, y + 16, Pixel {r: 128, g: 6, b: 45,});
        }
    }
    for x in 48..56 {
        for y in 104..160 {
            img.set_pixel(x + 32, y + 16, Pixel {r: 128, g: 6, b: 45,});
        }
    }
    for x in 56..88 {
        for y in 120..136 {
            img.set_pixel(x + 32, y + 16, Pixel {r: 128, g: 6, b: 45,});
        }
    }
    for x in 88..96 {
        for y in 120..160 {
            img.set_pixel(x + 32, y + 16, Pixel {r: 128, g: 6, b: 45,});
        }
    }
    for x in 96..104 {
        for y in 112..160 {
            img.set_pixel(x + 32, y + 16, Pixel {r: 128, g: 6, b: 45,});
        }
    }
    for x in 104..112 {
        for y in 96..160 {
            img.set_pixel(x + 32, y + 16, Pixel {r: 128, g: 6, b: 45,});
        }
    }

    // rgb(148, 200, 219)
    for x in 64..80 {
        for y in 32..56 {
            img.set_pixel(x + 32, y + 16, Pixel {r: 148, g: 200, b: 219,});
        }
    }
    for x in 80..88 {
        for y in 40..48 {
            img.set_pixel(x + 32, y + 16, Pixel {r: 148, g: 200, b: 219,});
        }
    }
    for x in 80..112 {
        for y in 48..64 {
            img.set_pixel(x + 32, y + 16, Pixel {r: 148, g: 200, b: 219,});
        }
    }
    for x in 112..120 {
        for y in 40..56 {
            img.set_pixel(x + 32, y + 16, Pixel {r: 148, g: 200, b: 219,});
        }
    }

    // rgb(69, 103, 105)
    for x in 56..64 {
        for y in 40..64 {
            img.set_pixel(x + 32, y + 16, Pixel {r: 69, g: 103, b: 105,});
        }
    }
    for x in 64..80 {
        for y in 56..72 {
            img.set_pixel(x + 32, y + 16, Pixel {r: 69, g: 103, b: 105,});
        }
    }
    for x in 80..112 {
        for y in 64..72 {
            img.set_pixel(x + 32, y + 16, Pixel {r: 69, g: 103, b: 105,});
        }
    }
    for x in 112..120 {
        for y in 56..64 {
            img.set_pixel(x + 32, y + 16, Pixel {r: 69, g: 103, b: 105,});
        }
    }

    let _ = img.save("img.bmp");
}

use std::io::Write;

fn main() {
    print!("What is your name? ");
    std::io::stdout().flush().expect("Failed to flush output");
    let mut name = String::new();
    let _ = std::io::stdin().read_line(&mut name);
    if name.trim().is_empty() {
        println!("No name entered :(, goodbye.")
    } else {
        println!("Hello, {}, nice to meet you!", name.trim())
    }
}

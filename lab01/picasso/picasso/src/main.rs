fn main() {
    for arg in std::env::args().skip(1) {
        println!("===== {arg} =====");
        match bmp::open(&arg) {
            Err(err) => println!("Error! {:?}", err),
            Ok(img) => {
                for (x, y) in img.coordinates() {
                    match img.get_pixel(x, y) {
                        bmp::consts::RED => {print!("R ")},
                        bmp::consts::LIME => {print!("G ")},
                        bmp::consts::BLUE => {print!("B ")},
                        bmp::consts::WHITE => {print!("W ")},
                        _ => {}
                    }

                    if x == img.get_width() - 1 {
                        println!();
                    }
                }
            }
        }
    }
}

fn main() {
    let mut vec = vec![1, 2, 3, 4, 5, 6, 7, 8, 9, 10];

    // Program doesn't compile as only one mutable borrow allowed at a time
    let a = &mut vec;
    //let b = &mut vec;
    // The second mutable borrow was removed, as only the first is needed

    a.push(11);
    a.push(12);
}

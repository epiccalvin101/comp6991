use serde::Deserialize;
use std::collections::VecDeque;
use std::io;

#[derive(Debug, Deserialize)]
enum Instruction {
    Set(i32),
    Left,
    Right,
    Reset,
}

#[derive(Debug, Default)]
struct Light {
    // TODO: change me!
    left: Option<Box<Light>>,
    right:Option<Box<Light>>,
    brightness: i32,
}

impl Light {
    fn sum_brightness(&self) -> i32 {
        let left_sum = self.left.as_ref().map_or(0, |left| left.sum_brightness());
        let right_sum = self.right.as_ref().map_or(0, |right| right.sum_brightness());

        self.brightness + left_sum + right_sum
    }

    fn count_nodes(&self) -> i32 {
        let left_count = self.left.as_ref().map_or(0, |left| left.count_nodes());
        let right_count = self.right.as_ref().map_or(0, |right| right.count_nodes());

        1 + left_count + right_count
    }

}

fn get_instructions_from_stdin() -> VecDeque<Instruction> {
    let mut instructions = String::new();
    io::stdin().read_line(&mut instructions).unwrap();
    ron::from_str(&instructions).unwrap()
}

fn main() {
    let mut instructions = get_instructions_from_stdin();
    let mut light = Light { left: None, right: None, brightness: 0};
    //println!("{instructions:?}");
    //println!("{light:?}");
    // TODO: your implementation here
    let mut cur = &mut light;
    while let Some(instruction) = instructions.pop_front() {
        match instruction {
            Instruction::Set(bright) => cur.brightness = bright,
            Instruction::Left => cur = cur.left.get_or_insert_with(Default::default),
            Instruction::Right => cur = cur.right.get_or_insert_with(Default::default),
            Instruction::Reset => cur = &mut light,
        }
    }

    println!("{}", light.sum_brightness()/light.count_nodes());
}

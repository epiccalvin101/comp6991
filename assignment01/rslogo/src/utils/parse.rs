/// Gets all non-empty and non-comment lines from the file.
/// Returns None on error
pub fn parse_file(file_path: &std::path::PathBuf) -> Option<Vec<String>> {
    let content = match std::fs::read_to_string(file_path) {
        Ok(content) => content,
        Err(err) => {
            eprintln!("Error reading the input file, {err}");
            return None;
        }
    };
    let lines: Vec<String> = content
        .lines()
        .filter(|line| !line.trim().is_empty() && !line.trim().starts_with("//"))
        .map(String::from)
        .collect();

    // Check if braces are balanced
    let mut brace_balance = 0;
    for string in &lines {
        if let Some("]") = string.split_whitespace().next() {
            brace_balance -= 1;
            if brace_balance < 0 {
                eprintln!("Braces are unbalanced in logo file");
                return None;
            }
        }
        if let Some("[") = string.split_whitespace().last() {
            brace_balance += 1;
        }
    }
    if brace_balance != 0 {
        eprintln!("Braces are unbalanced in logo file");
        return None;
    }

    // Check if procedure TO..END are balanced
    let mut proc_balance = 0;
    for string in &lines {
        if let Some(c) = string.split_whitespace().next() {
            match c {
                "END" => {
                    proc_balance -= 1;
                    if proc_balance < 0 {
                        eprintln!("Procedure TO..END definitions are unbalanced in logo file");
                        return None;
                    }
                }
                "TO" => {
                    proc_balance += 1;
                    if proc_balance > 1 {
                        eprintln!("Procedures cannot contain other procedures in logo file");
                        return None;
                    }
                }
                _ => {}
            }
        }
    }
    if proc_balance != 0 {
        eprintln!("Procedure TO..END definitions are unbalanced in logo file");
        return None;
    }

    Some(lines)
}

/// Helper function to skip to the correct 'nest'
pub fn skip_braces(index: &usize, lines: &Vec<String>) -> usize {
    let mut braces = 1;
    let mut i = index + 1;
    while i < lines.len() {
        if let Some(c) = lines[i].split_whitespace().last() {
            match c {
                "[" => {
                    braces += 1;
                }
                "]" => {
                    braces -= 1;
                    if braces == 0 {
                        break;
                    }
                }
                _ => {}
            }
        }

        i += 1;
    }

    i
}

/// Helper function to skip procedure lines
pub fn skip_procedure(index: &usize, lines: &Vec<String>) -> usize {
    let mut i = index + 1;
    while i < lines.len() {
        if let Some("END") = lines[i].split_whitespace().last() {
            break;
        }

        i += 1;
    }

    i
}

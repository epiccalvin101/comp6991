/// Special return type enum for categorising logo terms
pub enum ResultValue {
    Float(f32),
    Boolean(bool),
    String(String),
    None,
}

impl PartialEq for ResultValue {
    fn eq(&self, other: &Self) -> bool {
        match (self, other) {
            (ResultValue::Float(a), ResultValue::Float(b)) => a == b,
            (ResultValue::Boolean(a), ResultValue::Boolean(b)) => a == b,
            (ResultValue::String(a), ResultValue::String(b)) => a == b,
            (ResultValue::None, ResultValue::None) => true,
            _ => false,
        }
    }
}

impl ToString for ResultValue {
    fn to_string(&self) -> String {
        match self {
            ResultValue::Float(val) => val.to_string(),
            ResultValue::Boolean(val) => val.to_string(),
            ResultValue::String(val) => val.to_string(),
            ResultValue::None => "".to_string(),
        }
    }
}

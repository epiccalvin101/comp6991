use unsvg::{get_end_coordinates, Color, Image};
pub struct Turtle<'a> {
    x: f32,
    y: f32,
    facing: f32,
    pen_down: bool,
    colour: Color,
    colour_num: usize,
    image: &'a mut Image,
}

impl Turtle<'_> {
    pub fn new(xpos: f32, ypos: f32, image: &mut Image) -> Turtle {
        Turtle {
            x: xpos,
            y: ypos,
            facing: 0.0,
            pen_down: false,
            colour: Color::white(),
            colour_num: 7,
            image,
        }
    }

    pub fn pen_up(&mut self) {
        self.pen_down = false;
    }

    pub fn pen_down(&mut self) {
        self.pen_down = true;
    }

    fn do_draw(&mut self, direction: i32, length: f32) -> (f32, f32) {
        if self.is_pen_down() {
            return self
                .image
                .draw_simple_line(self.x, self.y, direction, length, self.colour)
                .unwrap_or_else(|e| {
                    eprintln!("Error while drawing line with draw_simple_line: {e}");
                    std::process::exit(1);
                });
        }
        get_end_coordinates(self.x, self.y, direction, length)
    }

    pub fn forward(&mut self, pixels: f32) {
        let (newx, newy) = self.do_draw(self.facing as i32, pixels);
        self.x = newx;
        self.y = newy;
    }

    pub fn back(&mut self, pixels: f32) {
        self.forward(-pixels);
    }

    pub fn right(&mut self, pixels: f32) {
        let (newx, newy) = self.do_draw(self.facing as i32 + 90, pixels);
        self.x = newx;
        self.y = newy;
    }

    pub fn left(&mut self, pixels: f32) {
        self.right(-pixels)
    }

    pub fn set_pen_colour(&mut self, col: usize) {
        self.colour = unsvg::COLORS[col];
        self.colour_num = col;
    }

    pub fn turn(&mut self, deg: f32) {
        self.facing += deg;
    }

    pub fn set_heading(&mut self, deg: f32) {
        self.facing = deg;
    }

    pub fn set_x(&mut self, xnew: f32) {
        self.x = xnew;
    }

    pub fn set_y(&mut self, ynew: f32) {
        self.y = ynew;
    }

    pub fn get_x(&self) -> f32 {
        self.x
    }

    pub fn get_y(&self) -> f32 {
        self.y
    }

    pub fn get_facing(&self) -> f32 {
        self.facing
    }

    pub fn is_pen_down(&self) -> bool {
        self.pen_down
    }

    pub fn get_colour_num(&self) -> usize {
        self.colour_num
    }
}

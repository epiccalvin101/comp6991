use super::command::{call_command, handle};
use crate::{turtle::Turtle, ResultValue};
use std::collections::HashMap;

/// function for IF
pub fn if_fn(
    lines: &Vec<String>,
    index: &usize,
    variables: &mut HashMap<String, ResultValue>,
    turtle: &mut Turtle,
) {
    let tokens = &mut lines[*index].split_whitespace().peekable();
    tokens.next();

    if tokens.peek().is_none() {
        eprintln!("IF needs one argument");
        std::process::exit(1)
    }

    let val = if let Some(ResultValue::Boolean(result)) = handle(tokens, variables, turtle) {
        result
    } else {
        eprintln!("Invalid value for IF command. Argument must result in a boolean");
        std::process::exit(1);
    };

    if tokens.next() != Some("[") {
        eprintln!("IF statement must have opening brace '['");
        std::process::exit(1)
    } else if let Some(val) = tokens.next() {
        eprintln!("Cannot have tokens after opening brace '['\n Unused tokens: {val}");
        tokens.for_each(|tok| eprint!(" {tok}"));

        std::process::exit(1);
    }

    if val {
        call_command(lines, &(index + 1), variables, turtle);
    }
}

/// function for WHILE
pub fn while_fn(
    lines: &Vec<String>,
    index: &usize,
    variables: &mut HashMap<String, ResultValue>,
    turtle: &mut Turtle,
) {
    let tokens = &mut lines[*index].split_whitespace().peekable();
    tokens.next();

    if tokens.peek().is_none() {
        eprintln!("WHILE needs one argument");
        std::process::exit(1)
    }

    let val = if let Some(ResultValue::Boolean(result)) = handle(tokens, variables, turtle) {
        result
    } else {
        eprintln!("Invalid value for WHILE command. Argument must result in a boolean");
        std::process::exit(1);
    };

    if tokens.next() != Some("[") {
        eprintln!("WHILE statement must have opening brace '['");
        std::process::exit(1)
    } else if let Some(val) = tokens.next() {
        eprintln!("Cannot have tokens after opening brace '['\n Unused tokens: {val}");
        tokens.for_each(|tok| eprint!(" {tok}"));

        std::process::exit(1);
    }

    if val {
        call_command(lines, &(index + 1), variables, turtle);
        while_fn(lines, index, variables, turtle)
    }
}

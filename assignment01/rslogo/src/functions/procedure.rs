use super::command::{call_command, handle};
use super::variables::make;
use crate::{turtle::Turtle, ResultValue};
use std::collections::HashMap;

/// function for TO, creates procedure with given name
pub fn to(lines: &[String], index: &usize, variables: &mut HashMap<String, ResultValue>) {
    let tokens = &mut lines[*index].split_whitespace().peekable();
    tokens.next();

    if tokens.peek().is_none() {
        eprintln!("TO needs a procedure name");
        std::process::exit(1)
    }

    if let Some(name) = tokens.next() {
        variables.insert(format!("{} ", name), ResultValue::Float(*index as f32));
    }
}

/// function to do a specified procedure created by TO
pub fn do_procedure(
    lines: &Vec<String>,
    index: &usize,
    variables: &mut HashMap<String, ResultValue>,
    turtle: &mut Turtle,
) {
    let arg_tokens = &mut lines[*index].split_whitespace().peekable();

    let proc_index = if let Some(ResultValue::Float(result)) = variables.get(&format!(
        "{} ",
        arg_tokens.next().unwrap_or_else(|| {
            std::process::exit(1);
        })
    )) {
        *result as usize
    } else {
        std::process::exit(1);
    };

    let proc_tokens = &mut lines[proc_index].split_whitespace().peekable();
    proc_tokens.next();
    let proc_name = proc_tokens.next().unwrap_or_else(|| {
        std::process::exit(1);
    });

    let mut proc_variables: HashMap<String, ResultValue> = Default::default();
    while let (Some(name), Some(value)) =
        (proc_tokens.next(), handle(arg_tokens, variables, turtle))
    {
        make(
            &mut format!("{name} \"{}", value.to_string())
                .split_whitespace()
                .peekable(),
            &mut proc_variables,
            turtle,
        );
    }

    if proc_tokens.next().is_some() || arg_tokens.next().is_some() {
        eprintln!("Mismatched argument count for procedure {proc_name}");
        std::process::exit(1)
    }

    call_command(lines, &(proc_index + 1), &mut proc_variables, turtle);
}

use super::command::handle;
use crate::{turtle::Turtle, ResultValue};
use std::{collections::HashMap, iter::Peekable};

/// function for MAKE
pub fn make(
    tokens: &mut Peekable<std::str::SplitWhitespace<'_>>,
    variables: &mut HashMap<String, ResultValue>,
    turtle: &mut Turtle,
) {
    if tokens.peek().is_none() {
        eprintln!("MAKE needs two arguments");
        std::process::exit(1)
    }

    let name = if let Some(ResultValue::String(new_name)) = handle(tokens, variables, turtle) {
        new_name
    } else {
        eprintln!("Invalid value for MAKE command. First argument must be a nonspecial string");
        std::process::exit(1)
    };

    if tokens.peek().is_none() {
        eprintln!("MAKE needs two arguments");
        std::process::exit(1)
    }

    let val = if let Some(result) = handle(tokens, variables, turtle) {
        result
    } else {
        eprintln!("Invalid value for MAKE command. First argument must be a nonspecial string");
        std::process::exit(1)
    };

    if tokens.peek().is_some() {
        eprint!("MAKE takes two arguments. Unused:");
        tokens.for_each(|token| eprint!(" {token}"));

        std::process::exit(1)
    }

    variables.insert(name, val);
}

/// function for ADDASSIGN
pub fn add_assign(
    tokens: &mut Peekable<std::str::SplitWhitespace<'_>>,
    variables: &mut HashMap<String, ResultValue>,
    turtle: &mut Turtle,
) {
    if tokens.peek().is_none() {
        eprintln!("ADDASSIGN needs two arguments");
        std::process::exit(1)
    }

    let name: String;
    let val1: f32;
    match handle(tokens, variables, turtle) {
        Some(ResultValue::String(_name)) => match variables.get(&_name) {
            Some(ResultValue::Float(num)) => {
                name = _name.to_string();
                val1 = *num;
            }
            None => {
                eprintln!("Invalid value for ADDASSIGN command. First argument must be an existing variable");
                std::process::exit(1)
            }
            _ => {
                eprintln!("Invalid value for ADDASSIGN command. First variable must be assigned to a number");
                std::process::exit(1)
            }
        },
        _ => {
            eprintln!(
                "Invalid value for ADDASSIGN command. First argument must be a variable name"
            );
            std::process::exit(1)
        }
    }

    if tokens.peek().is_none() {
        eprintln!("MAKE needs two arguments");
        std::process::exit(1)
    }

    let val2 = if let Some(ResultValue::Float(num)) = handle(tokens, variables, turtle) {
        num
    } else {
        eprintln!("Invalid value for ADDASSIGN command. Second argument must be a number");
        std::process::exit(1)
    };

    if tokens.peek().is_some() {
        eprint!("ADDASSIGN takes two arguments. Unused:");
        tokens.for_each(|token| eprint!(" {token}"));

        std::process::exit(1)
    }

    variables.insert(name, ResultValue::Float(val1 + val2));
}

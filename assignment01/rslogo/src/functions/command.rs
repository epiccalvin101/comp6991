use crate::{turtle::Turtle, utils, ResultValue};
use std::collections::HashMap;
use std::iter::Peekable;

use super::comparison::{and, eq_fn, gt_fn, lt_fn, ne_fn, or};
use super::conditional::{if_fn, while_fn};
use super::control::{
    back, forward, left, pen_down, pen_up, right, set_heading, set_pen_colour, set_x, set_y, turn,
};
use super::math::{add, div, mul, sub};
use super::procedure::{do_procedure, to};
use super::query::{colour, heading, x_cor, y_cor};
use super::variables::{add_assign, make};

/// Function to call logo file commands line by line
pub fn call_command(
    lines: &Vec<String>,
    index: &usize,
    variables: &mut HashMap<String, ResultValue>,
    turtle: &mut Turtle,
) {
    if index >= &lines.len() {
        return;
    }

    if let Some(word) = lines[*index].split_whitespace().next() {
        match word {
            "IF" => {
                if_fn(lines, index, variables, turtle);
                return call_command(
                    lines,
                    &(utils::parse::skip_braces(index, lines) + 1),
                    variables,
                    turtle,
                );
            }
            "WHILE" => {
                while_fn(lines, index, variables, turtle);
                return call_command(
                    lines,
                    &(utils::parse::skip_braces(index, lines) + 1),
                    variables,
                    turtle,
                );
            }
            "TO" => {
                to(lines, index, variables);
                return call_command(
                    lines,
                    &(utils::parse::skip_procedure(index, lines) + 1),
                    variables,
                    turtle,
                );
            }
            _ => {
                if variables.contains_key(&format!("{word} ")) {
                    do_procedure(lines, index, variables, turtle);
                } else if let Some(ResultValue::None) = handle(
                    &mut lines[*index].split_whitespace().peekable(),
                    variables,
                    turtle,
                ) {
                    return;
                }
            }
        }
    };

    call_command(lines, &(index + 1), variables, turtle)
}

/// Function for handling a line, calling the specified commands or getting needed values
pub fn handle(
    tokens: &mut Peekable<std::str::SplitWhitespace<'_>>,
    variables: &mut HashMap<String, ResultValue>,
    turtle: &mut Turtle,
) -> Option<ResultValue> {
    if let Some(word) = tokens.next() {
        // Prefix with " means a Value
        // Return the f32, boolean, or string value
        if word.starts_with('"') {
            return handle_value(word);
        }
        // Prefix with : means a Variable
        // Return the value of the variable
        else if word.starts_with(':') {
            return handle_variable(word, variables);
        }
        // No prefix special words means a command or query
        match word {
            "PENUP" => {
                pen_up(tokens, turtle);
            }
            "PENDOWN" => {
                pen_down(tokens, turtle);
            }
            "FORWARD" => {
                forward(tokens, variables, turtle);
            }
            "BACK" => {
                back(tokens, variables, turtle);
            }
            "LEFT" => {
                left(tokens, variables, turtle);
            }
            "RIGHT" => {
                right(tokens, variables, turtle);
            }
            "SETPENCOLOR" => {
                set_pen_colour(tokens, variables, turtle);
            }
            "TURN" => {
                turn(tokens, variables, turtle);
            }
            "SETHEADING" => {
                set_heading(tokens, variables, turtle);
            }
            "SETX" => {
                set_x(tokens, variables, turtle);
            }
            "SETY" => {
                set_y(tokens, variables, turtle);
            }
            "MAKE" => {
                make(tokens, variables, turtle);
            }
            "ADDASSIGN" => {
                add_assign(tokens, variables, turtle);
            }
            "XCOR" => {
                return Some(x_cor(turtle));
            }
            "YCOR" => {
                return Some(y_cor(turtle));
            }
            "HEADING" => {
                return Some(heading(turtle));
            }
            "COLOR" => {
                return Some(colour(turtle));
            }
            "EQ" => {
                return Some(eq_fn(tokens, variables, turtle));
            }
            "NE" => {
                return Some(ne_fn(tokens, variables, turtle));
            }
            "GT" => {
                return Some(gt_fn(tokens, variables, turtle));
            }
            "LT" => {
                return Some(lt_fn(tokens, variables, turtle));
            }
            "AND" => {
                return Some(and(tokens, variables, turtle));
            }
            "OR" => {
                return Some(or(tokens, variables, turtle));
            }
            "+" => {
                return Some(add(tokens, variables, turtle));
            }
            "-" => {
                return Some(sub(tokens, variables, turtle));
            }
            "*" => {
                return Some(mul(tokens, variables, turtle));
            }
            "/" => {
                return Some(div(tokens, variables, turtle));
            }
            "]" => {
                return Some(ResultValue::None);
            }
            "END" => {
                return Some(ResultValue::None);
            }
            err => {
                eprintln!(
                    "Unrecognised word is not a command, variable, value, or procedure: {err}"
                );
                std::process::exit(1);
            }
        }
    };

    None
}

/// Function to return value of a word
fn handle_value(word: &str) -> Option<ResultValue> {
    if word.chars().count() <= 1 {
        eprintln!("Value cannot be empty: \"");
        std::process::exit(1);
    }

    let sub = &word[1..];
    match sub.parse::<f32>() {
        Ok(num) => Some(ResultValue::Float(num)),
        Err(_) => match sub {
            "TRUE" => Some(ResultValue::Boolean(true)),
            "FALSE" => Some(ResultValue::Boolean(false)),
            _ => Some(ResultValue::String(sub.to_string())),
        },
    }
}

/// Function to return the value of a variable
fn handle_variable(
    word: &str,
    variables: &mut HashMap<String, ResultValue>,
) -> Option<ResultValue> {
    if word.chars().count() <= 1 {
        eprintln!("Variable name cannot be empty: :");
        std::process::exit(1);
    }

    let sub = &word[1..];
    match variables.get(sub) {
        Some(ResultValue::Float(num)) => Some(ResultValue::Float(*num)),
        Some(ResultValue::Boolean(bool)) => Some(ResultValue::Boolean(*bool)),
        Some(ResultValue::String(string)) => Some(ResultValue::String(string.to_string())),
        _ => {
            eprintln!("Variable does not exist: :{sub} ");
            std::process::exit(1);
        }
    }
}

use super::command::handle;
use crate::{turtle::Turtle, ResultValue};
use std::{collections::HashMap, iter::Peekable};

/// Handle PENUP command and errors
pub fn pen_up(tokens: &mut Peekable<std::str::SplitWhitespace<'_>>, turtle: &mut Turtle) {
    if tokens.peek().is_some() {
        eprint!("PENUP takes no arguments: PENUP");
        tokens.for_each(|token| eprint!(" {token}"));

        std::process::exit(1)
    }

    turtle.pen_up();
}

/// Handle PENDOWN command and errors
pub fn pen_down(tokens: &mut Peekable<std::str::SplitWhitespace<'_>>, turtle: &mut Turtle) {
    if tokens.peek().is_some() {
        eprint!("PENDOWN takes no arguments: PENDOWN");
        tokens.for_each(|token| eprint!(" {token}"));

        std::process::exit(1)
    }

    turtle.pen_down();
}

/// Handle FORWARD command and errors
pub fn forward(
    tokens: &mut Peekable<std::str::SplitWhitespace<'_>>,
    variables: &mut HashMap<String, ResultValue>,
    turtle: &mut Turtle,
) {
    if tokens.peek().is_none() {
        eprintln!("FORWARD needs one argument");
        std::process::exit(1)
    }

    match handle(tokens, variables, turtle) {
        Some(ResultValue::Float(num)) => {
            turtle.forward(num);
        }
        _ => {
            eprintln!("Invalid value for FORWARD command. Must be a number");
            std::process::exit(1)
        }
    }

    if tokens.peek().is_some() {
        eprint!("FORWARD takes one argument. Unused: ");
        tokens.for_each(|token| eprint!("{token} "));

        std::process::exit(1)
    }
}

/// Handle BACK command and errors
pub fn back(
    tokens: &mut Peekable<std::str::SplitWhitespace<'_>>,
    variables: &mut HashMap<String, ResultValue>,
    turtle: &mut Turtle,
) {
    if tokens.peek().is_none() {
        eprintln!("BACK needs one argument");
        std::process::exit(1)
    }

    match handle(tokens, variables, turtle) {
        Some(ResultValue::Float(num)) => {
            turtle.back(num);
        }
        _ => {
            eprintln!("Invalid value for BACK command. Must be a number");
            std::process::exit(1)
        }
    }

    if tokens.peek().is_some() {
        eprint!("BACK takes one argument. Unused: ");
        tokens.for_each(|token| eprint!("{token} "));

        std::process::exit(1)
    }
}

/// Handle RIGHT command and errors
pub fn right(
    tokens: &mut Peekable<std::str::SplitWhitespace<'_>>,
    variables: &mut HashMap<String, ResultValue>,
    turtle: &mut Turtle,
) {
    if tokens.peek().is_none() {
        eprintln!("RIGHT needs one argument");
        std::process::exit(1)
    }

    match handle(tokens, variables, turtle) {
        Some(ResultValue::Float(num)) => {
            turtle.right(num);
        }
        _ => {
            eprintln!("Invalid value for RIGHT command. Must be a number");
            std::process::exit(1)
        }
    }

    if tokens.peek().is_some() {
        eprint!("RIGHT takes one argument. Unused: ");
        tokens.for_each(|token| eprint!("{token} "));

        std::process::exit(1)
    }
}

/// Handle LEFT command and errors
pub fn left(
    tokens: &mut Peekable<std::str::SplitWhitespace<'_>>,
    variables: &mut HashMap<String, ResultValue>,
    turtle: &mut Turtle,
) {
    if tokens.peek().is_none() {
        eprintln!("LEFT needs one argument");
        std::process::exit(1)
    }

    match handle(tokens, variables, turtle) {
        Some(ResultValue::Float(num)) => {
            turtle.left(num);
        }
        _ => {
            eprintln!("Invalid value for LEFT command. Must be a number");
            std::process::exit(1)
        }
    }

    if tokens.peek().is_some() {
        eprint!("LEFT takes one argument. Unused: ");
        tokens.for_each(|token| eprint!("{token} "));

        std::process::exit(1)
    }
}

/// Handle SETPENCOLOR command and errors
pub fn set_pen_colour(
    tokens: &mut Peekable<std::str::SplitWhitespace<'_>>,
    variables: &mut HashMap<String, ResultValue>,
    turtle: &mut Turtle,
) {
    if tokens.peek().is_none() {
        eprintln!("SETPENCOLOR needs one argument");
        std::process::exit(1)
    }

    match handle(tokens, variables, turtle) {
        Some(ResultValue::Float(num)) => {
            if (0.0..=15.0).contains(&num) && num.fract() == 0.0 {
                turtle.set_pen_colour(num as usize);
            } else {
                eprintln!("SETPENCOLOR takes integer values [0, 15]");
                std::process::exit(1)
            }
        }
        _ => {
            eprintln!("Invalid value for SETPENCOLOR command. Must be an integer [0, 15]");
            std::process::exit(1)
        }
    }

    if tokens.peek().is_some() {
        eprint!("SETPENCOLOR takes one argument. Unused: ");
        tokens.for_each(|token| eprint!("{token} "));

        std::process::exit(1)
    }
}

/// Handle TURN command and errors
pub fn turn(
    tokens: &mut Peekable<std::str::SplitWhitespace<'_>>,
    variables: &mut HashMap<String, ResultValue>,
    turtle: &mut Turtle,
) {
    if tokens.peek().is_none() {
        eprintln!("TURN needs one argument");
        std::process::exit(1)
    }

    match handle(tokens, variables, turtle) {
        Some(ResultValue::Float(num)) => {
            turtle.turn(num);
        }
        _ => {
            eprintln!("Invalid value for TURN command. Must be a number");
            std::process::exit(1)
        }
    }

    if tokens.peek().is_some() {
        eprint!("TURN takes one argument. Unused: ");
        tokens.for_each(|token| eprint!("{token} "));

        std::process::exit(1)
    }
}

/// Handle SETHEADING command and errors
pub fn set_heading(
    tokens: &mut Peekable<std::str::SplitWhitespace<'_>>,
    variables: &mut HashMap<String, ResultValue>,
    turtle: &mut Turtle,
) {
    if tokens.peek().is_none() {
        eprintln!("SETHEADING needs one argument");
        std::process::exit(1)
    }

    match handle(tokens, variables, turtle) {
        Some(ResultValue::Float(num)) => {
            turtle.set_heading(num);
        }
        _ => {
            eprintln!("Invalid value for SETHEADING command. Must be a number");
            std::process::exit(1)
        }
    }

    if tokens.peek().is_some() {
        eprint!("SETHEADING takes one argument. Unused: ");
        tokens.for_each(|token| eprint!("{token} "));

        std::process::exit(1)
    }
}

/// Handle SETX command and errors
pub fn set_x(
    tokens: &mut Peekable<std::str::SplitWhitespace<'_>>,
    variables: &mut HashMap<String, ResultValue>,
    turtle: &mut Turtle,
) {
    if tokens.peek().is_none() {
        eprintln!("SETX needs one argument");
        std::process::exit(1)
    }

    match handle(tokens, variables, turtle) {
        Some(ResultValue::Float(num)) => {
            turtle.set_x(num);
        }
        _ => {
            eprintln!("Invalid value for SETX command. Must be a number");
            std::process::exit(1)
        }
    }

    if tokens.peek().is_some() {
        eprint!("SETX takes one argument. Unused: ");
        tokens.for_each(|token| eprint!("{token} "));

        std::process::exit(1)
    }
}

/// Handle SETY command and errors
pub fn set_y(
    tokens: &mut Peekable<std::str::SplitWhitespace<'_>>,
    variables: &mut HashMap<String, ResultValue>,
    turtle: &mut Turtle,
) {
    if tokens.peek().is_none() {
        eprintln!("SETY needs one argument");
        std::process::exit(1)
    }

    match handle(tokens, variables, turtle) {
        Some(ResultValue::Float(num)) => {
            turtle.set_y(num);
        }
        _ => {
            eprintln!("Invalid value for SETY command. Must be a number");
            std::process::exit(1)
        }
    }

    if tokens.peek().is_some() {
        eprint!("SETY takes one argument. Unused: ");
        tokens.for_each(|token| eprint!("{token} "));

        std::process::exit(1)
    }
}

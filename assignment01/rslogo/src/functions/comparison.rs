use super::command::handle;
use crate::{turtle::Turtle, ResultValue};
use std::{collections::HashMap, iter::Peekable};

/// function for EQ
pub fn eq_fn(
    tokens: &mut Peekable<std::str::SplitWhitespace<'_>>,
    variables: &mut HashMap<String, ResultValue>,
    turtle: &mut Turtle,
) -> ResultValue {
    if tokens.peek().is_none() {
        eprintln!("EQ needs two arguments");
        std::process::exit(1)
    }

    let val1 = if let Some(result) = handle(tokens, variables, turtle) {
        result
    } else {
        eprintln!("Invalid value for EQ command. First argument must be a word or number");
        std::process::exit(1);
    };

    if tokens.peek().is_none() {
        eprintln!("EQ needs two arguments");
        std::process::exit(1)
    }

    let val2 = if let Some(result) = handle(tokens, variables, turtle) {
        result
    } else {
        eprintln!("Invalid value for EQ command. Second argument must be a word or number");
        std::process::exit(1);
    };

    ResultValue::Boolean(val1 == val2)
}

/// function for NE
pub fn ne_fn(
    tokens: &mut Peekable<std::str::SplitWhitespace<'_>>,
    variables: &mut HashMap<String, ResultValue>,
    turtle: &mut Turtle,
) -> ResultValue {
    if tokens.peek().is_none() {
        eprintln!("NE needs two arguments");
        std::process::exit(1)
    }

    let val1 = if let Some(result) = handle(tokens, variables, turtle) {
        result
    } else {
        eprintln!("Invalid value for NE command. First argument must be a word or number");
        std::process::exit(1);
    };

    if tokens.peek().is_none() {
        eprintln!("NE needs two arguments");
        std::process::exit(1)
    }

    let val2 = if let Some(result) = handle(tokens, variables, turtle) {
        result
    } else {
        eprintln!("Invalid value for NE command. Second argument must be a word or number");
        std::process::exit(1);
    };

    ResultValue::Boolean(val1 != val2)
}

/// function for GT
pub fn gt_fn(
    tokens: &mut Peekable<std::str::SplitWhitespace<'_>>,
    variables: &mut HashMap<String, ResultValue>,
    turtle: &mut Turtle,
) -> ResultValue {
    if tokens.peek().is_none() {
        eprintln!("GT needs two arguments");
        std::process::exit(1)
    }

    let val1 = if let Some(ResultValue::Float(result)) = handle(tokens, variables, turtle) {
        result
    } else {
        eprintln!("Invalid value for GT command. First argument must result in a number");
        std::process::exit(1);
    };

    if tokens.peek().is_none() {
        eprintln!("GT needs two arguments");
        std::process::exit(1)
    }

    let val2 = if let Some(ResultValue::Float(result)) = handle(tokens, variables, turtle) {
        result
    } else {
        eprintln!("Invalid value for GT command. Second argument must result in a number");
        std::process::exit(1);
    };

    ResultValue::Boolean(val1 > val2)
}

/// function for LT
pub fn lt_fn(
    tokens: &mut Peekable<std::str::SplitWhitespace<'_>>,
    variables: &mut HashMap<String, ResultValue>,
    turtle: &mut Turtle,
) -> ResultValue {
    if tokens.peek().is_none() {
        eprintln!("LT needs two arguments");
        std::process::exit(1)
    }

    let val1 = if let Some(ResultValue::Float(result)) = handle(tokens, variables, turtle) {
        result
    } else {
        eprintln!("Invalid value for LT command. First argument must result in a number");
        std::process::exit(1);
    };

    if tokens.peek().is_none() {
        eprintln!("LT needs two arguments");
        std::process::exit(1)
    }

    let val2 = if let Some(ResultValue::Float(result)) = handle(tokens, variables, turtle) {
        result
    } else {
        eprintln!("Invalid value for LT command. Second argument must result in a number");
        std::process::exit(1);
    };

    ResultValue::Boolean(val1 < val2)
}

/// function for AND
pub fn and(
    tokens: &mut Peekable<std::str::SplitWhitespace<'_>>,
    variables: &mut HashMap<String, ResultValue>,
    turtle: &mut Turtle,
) -> ResultValue {
    if tokens.peek().is_none() {
        eprintln!("AND needs two arguments");
        std::process::exit(1)
    }

    let val1 = if let Some(ResultValue::Boolean(result)) = handle(tokens, variables, turtle) {
        result
    } else {
        eprintln!("Invalid value for AND command. First argument must result in a boolean");
        std::process::exit(1);
    };

    if tokens.peek().is_none() {
        eprintln!("AND needs two arguments");
        std::process::exit(1)
    }

    let val2 = if let Some(ResultValue::Boolean(result)) = handle(tokens, variables, turtle) {
        result
    } else {
        eprintln!("Invalid value for AND command. Second argument must result in a boolean");
        std::process::exit(1);
    };

    ResultValue::Boolean(val1 && val2)
}

/// function for OR
pub fn or(
    tokens: &mut Peekable<std::str::SplitWhitespace<'_>>,
    variables: &mut HashMap<String, ResultValue>,
    turtle: &mut Turtle,
) -> ResultValue {
    if tokens.peek().is_none() {
        eprintln!("OR needs two arguments");
        std::process::exit(1)
    }

    let val1 = if let Some(ResultValue::Boolean(result)) = handle(tokens, variables, turtle) {
        result
    } else {
        eprintln!("Invalid value for OR command. First argument must result in a boolean");
        std::process::exit(1);
    };

    if tokens.peek().is_none() {
        eprintln!("OR needs two arguments");
        std::process::exit(1)
    }

    let val2 = if let Some(ResultValue::Boolean(result)) = handle(tokens, variables, turtle) {
        result
    } else {
        eprintln!("Invalid value for OR command. Second argument must result in a boolean");
        std::process::exit(1);
    };

    ResultValue::Boolean(val1 || val2)
}

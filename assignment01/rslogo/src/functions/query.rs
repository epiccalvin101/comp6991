use crate::{turtle::Turtle, ResultValue};

/// function for XCOR
pub fn x_cor(turtle: &mut Turtle) -> ResultValue {
    ResultValue::Float(turtle.get_x())
}

/// function for XCOR
pub fn y_cor(turtle: &mut Turtle) -> ResultValue {
    ResultValue::Float(turtle.get_y())
}

/// function for HEADING
pub fn heading(turtle: &mut Turtle) -> ResultValue {
    ResultValue::Float(turtle.get_facing())
}

/// function for COLOR
pub fn colour(turtle: &mut Turtle) -> ResultValue {
    ResultValue::Float(turtle.get_colour_num() as f32)
}

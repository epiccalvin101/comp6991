use super::command::handle;
use crate::{turtle::Turtle, ResultValue};
use std::{collections::HashMap, iter::Peekable};

/// function for +
pub fn add(
    tokens: &mut Peekable<std::str::SplitWhitespace<'_>>,
    variables: &mut HashMap<String, ResultValue>,
    turtle: &mut Turtle,
) -> ResultValue {
    if tokens.peek().is_none() {
        eprintln!("+ needs two arguments");
        std::process::exit(1)
    }

    let val1 = if let Some(ResultValue::Float(result)) = handle(tokens, variables, turtle) {
        result
    } else {
        eprintln!("Invalid value for + command. First argument must result in a number");
        std::process::exit(1);
    };

    if tokens.peek().is_none() {
        eprintln!("+ needs two arguments");
        std::process::exit(1)
    }

    let val2 = if let Some(ResultValue::Float(result)) = handle(tokens, variables, turtle) {
        result
    } else {
        eprintln!("Invalid value for + command. Second argument must result in a number");
        std::process::exit(1);
    };

    ResultValue::Float(val1 + val2)
}

/// function for -
pub fn sub(
    tokens: &mut Peekable<std::str::SplitWhitespace<'_>>,
    variables: &mut HashMap<String, ResultValue>,
    turtle: &mut Turtle,
) -> ResultValue {
    if tokens.peek().is_none() {
        eprintln!("- needs two arguments");
        std::process::exit(1)
    }

    let val1 = if let Some(ResultValue::Float(result)) = handle(tokens, variables, turtle) {
        result
    } else {
        eprintln!("Invalid value for - command. First argument must result in a number");
        std::process::exit(1);
    };

    if tokens.peek().is_none() {
        eprintln!("- needs two arguments");
        std::process::exit(1)
    }

    let val2 = if let Some(ResultValue::Float(result)) = handle(tokens, variables, turtle) {
        result
    } else {
        eprintln!("Invalid value for - command. Second argument must result in a number");
        std::process::exit(1);
    };

    ResultValue::Float(val1 - val2)
}

/// function for *
pub fn mul(
    tokens: &mut Peekable<std::str::SplitWhitespace<'_>>,
    variables: &mut HashMap<String, ResultValue>,
    turtle: &mut Turtle,
) -> ResultValue {
    if tokens.peek().is_none() {
        eprintln!("* needs two arguments");
        std::process::exit(1)
    }

    let val1 = if let Some(ResultValue::Float(result)) = handle(tokens, variables, turtle) {
        result
    } else {
        eprintln!("Invalid value for * command. First argument must result in a number");
        std::process::exit(1);
    };

    if tokens.peek().is_none() {
        eprintln!("* needs two arguments");
        std::process::exit(1)
    }

    let val2 = if let Some(ResultValue::Float(result)) = handle(tokens, variables, turtle) {
        result
    } else {
        eprintln!("Invalid value for * command. Second argument must result in a number");
        std::process::exit(1);
    };

    ResultValue::Float(val1 * val2)
}

/// function for /
pub fn div(
    tokens: &mut Peekable<std::str::SplitWhitespace<'_>>,
    variables: &mut HashMap<String, ResultValue>,
    turtle: &mut Turtle,
) -> ResultValue {
    if tokens.peek().is_none() {
        eprintln!("/ needs two arguments");
        std::process::exit(1)
    }

    let val1 = if let Some(ResultValue::Float(result)) = handle(tokens, variables, turtle) {
        result
    } else {
        eprintln!("Invalid value for / command. First argument must result in a number");
        std::process::exit(1);
    };

    if tokens.peek().is_none() {
        eprintln!("/ needs two arguments");
        std::process::exit(1)
    }

    let val2 = if let Some(ResultValue::Float(result)) = handle(tokens, variables, turtle) {
        if result != 0.0 {
            result
        } else {
            eprintln!("Invalid value for / command. Second argument must be a non-zero number");
            std::process::exit(1);
        }
    } else {
        eprintln!("Invalid value for / command. Second argument must result in a number");
        std::process::exit(1);
    };

    ResultValue::Float(val1 / val2)
}

use clap::Parser;
use std::collections::HashMap;
use unsvg::Image;

mod functions;
mod turtle;
mod utils;

use functions::command::call_command;
use turtle::Turtle;
use utils::result::ResultValue;

/// A simple program to parse four arguments using clap.
#[derive(Parser)]
struct Args {
    /// Path to a file
    file_path: std::path::PathBuf,

    /// Path to an svg or png image
    image_path: std::path::PathBuf,

    /// Height
    height: u32,

    /// Width
    width: u32,
}

fn main() -> Result<(), ()> {
    let args: Args = Args::parse();

    // Access the parsed arguments
    let file_path = args.file_path;
    let image_path = args.image_path;
    let height = args.height;
    let width = args.width;

    let mut image = Image::new(width, height);

    // Parse given file
    let lines = utils::parse::parse_file(&file_path).unwrap_or_else(|| std::process::exit(1));

    // Create turtle to control pen
    let mut turtle = Turtle::new((width as f32) / 2.0, (height as f32) / 2.0, &mut image);

    // Use a hashmap for storing variables
    let mut variables: HashMap<String, ResultValue> = Default::default();

    // Go through logo file line by line
    call_command(&lines, &0, &mut variables, &mut turtle);

    // Save image
    match image_path.extension().and_then(|s| s.to_str()) {
        Some("svg") => {
            let res = image.save_svg(&image_path);
            if let Err(e) = res {
                eprintln!("Error saving svg: {e}");
                return Err(());
            }
        }
        Some("png") => {
            let res = image.save_png(&image_path);
            if let Err(e) = res {
                eprintln!("Error saving png: {e}");
                return Err(());
            }
        }
        _ => {
            eprintln!("File extension not supported");
            return Err(());
        }
    }

    Ok(())
}

use pars_libs::parse_line;
use std::process::Command;
use std::sync::{mpsc, Arc, Mutex};

type CommandReceiver = Arc<Mutex<mpsc::Receiver<Option<Vec<Vec<String>>>>>>;

struct Error {
    encountered: bool,
}

impl Error {
    fn did_encounter(&mut self) -> &bool {
        &self.encountered
    }

    fn set_encountered(&mut self, new: bool) {
        self.encountered = new;
    }
}

pub fn run_local(num_threads: i32, termination: &str) {
    // Command channel
    let (sender, receiver) = mpsc::channel();
    let receiver = Arc::new(Mutex::new(receiver));

    // Stdout channel
    let (stdout_sender, stdout_receiver) = mpsc::channel();
    let stdout_receiver = Arc::new(Mutex::new(stdout_receiver));

    // Error handling
    let error_struct = Arc::new(Mutex::new(Error { encountered: false }));

    std::thread::scope(|s| {
        // Spawn threads
        for _ in 0..num_threads {
            s.spawn(|| {
                run_thread(
                    Arc::clone(&receiver),
                    stdout_sender.clone(),
                    Arc::clone(&error_struct),
                    termination,
                )
            });
        }
        s.spawn(|| {
            stdout_thread(
                Arc::clone(&stdout_receiver),
                Arc::clone(&error_struct),
                termination,
            )
        });

        // Main loop to read and dispatch commands
        loop {
            let mut input = String::new();
            std::io::stdin().read_line(&mut input).unwrap();
            if input.is_empty() {
                break;
            }
            let line = parse_line(&input);

            // Send the command to a worker thread
            sender.send(line).unwrap();
        }

        // Signal worker threads to finish
        for _ in 0..num_threads {
            sender.send(None).unwrap();
        }
        stdout_sender.send(None).unwrap();
    });
}

fn run_thread(
    receiver: CommandReceiver,
    stdout_sender: mpsc::Sender<Option<Vec<String>>>,
    error_struct: Arc<Mutex<Error>>,
    termination: &str,
) {
    loop {
        // Receive a command from the main thread
        let commands = receiver.lock().unwrap().recv().unwrap();

        // Check for termination signal
        if commands.is_none() || *error_struct.lock().unwrap().did_encounter() {
            return;
        }

        let commands = commands.unwrap();

        let mut buffered_stdout = Vec::<String>::new();
        for command in commands {
            if *error_struct.lock().unwrap().did_encounter() && termination == "eager" {
                return;
            }

            // Execute the command
            let mut cmd = Command::new(&command[0]);
            for arg in &command[1..] {
                cmd.arg(arg);
            }

            let output = cmd.output().unwrap();

            // Error checking the command
            if !output.status.success() {
                match termination {
                    "never" => break,
                    "lazy" => {
                        error_struct.lock().unwrap().set_encountered(true);
                        return;
                    }
                    "eager" => {
                        error_struct.lock().unwrap().set_encountered(true);
                        return;
                    }
                    _ => {}
                }
            }

            // Buffer the stdout of the line
            buffered_stdout.push(String::from_utf8_lossy(&output.stdout).to_string());
        }

        // Send stdout after line is processed
        stdout_sender.send(Some(buffered_stdout)).unwrap();
    }
}

fn stdout_thread(
    stdout_receiver: Arc<Mutex<mpsc::Receiver<Option<Vec<String>>>>>,
    error_struct: Arc<Mutex<Error>>,
    termination: &str,
) {
    loop {
        let stdout = stdout_receiver.lock().unwrap().recv().unwrap();

        if stdout.is_none()
            || (*error_struct.lock().unwrap().did_encounter() && termination == "eager")
        {
            return;
        }

        for s in stdout.unwrap() {
            print!("{}", s);
        }
    }
}

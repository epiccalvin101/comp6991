use pars_libs::{Remote, RemoteCommand};
use std::sync::{mpsc, Arc, Mutex};
use std::{
    io::{BufRead, Write},
    process::Command,
    thread::JoinHandle,
};

pub fn start_remote(remote_string: &[String], termination: &str) {
    std::thread::scope(|s| {
        let channels: Vec<_> = remote_string
            .iter()
            .map(|_| {
                let (sender, receiver): (
                    mpsc::Sender<Option<String>>,
                    mpsc::Receiver<Option<String>>,
                ) = mpsc::channel();
                let receiver = Arc::new(Mutex::new(receiver));
                (sender, receiver)
            })
            .collect();

        for (remote, (_, receiver)) in remote_string.iter().zip(channels.clone().into_iter()) {
            s.spawn(move || run_remote(&remote.to_string(), termination, Arc::clone(&receiver)));
        }

        let mut channel_index = 0;
        loop {
            let mut input = String::new();
            std::io::stdin().read_line(&mut input).unwrap();
            if input.is_empty() {
                break;
            }

            let (sender, _) = &channels[channel_index];
            sender.send(Some(input)).unwrap();
            channel_index = (channel_index + 1) % channels.len();
        }

        for (sender, _) in channels.iter() {
            sender.send(None).unwrap();
        }
    });
}

fn run_remote(
    remote_string: &str,
    termination: &str,
    receiver: Arc<Mutex<mpsc::Receiver<Option<String>>>>,
) -> (JoinHandle<()>, JoinHandle<()>) {
    let (address, port, num_processes) =
        parse_address(remote_string).expect("Invalid remote address");

    let remote = Remote {
        addr: address.to_string(),
        port,
    };

    let mut remote_pars = Command::new("pars")
        .arg("-J")
        .arg(format!("{}", num_processes))
        .arg("-e")
        .arg(termination)
        .remote_spawn(&remote)
        .expect("Failed to start pars binary on remote");

    let stdin = remote_pars.stdin.take().unwrap();
    let stdout = remote_pars.stdout.take().unwrap();

    let input_thread = std::thread::spawn(|| remote_input(stdin, receiver));
    let output_thread = std::thread::spawn(|| remote_output(stdout));

    (input_thread, output_thread)
}

fn remote_input(
    mut stdin: std::process::ChildStdin,
    receiver: Arc<Mutex<mpsc::Receiver<Option<String>>>>,
) {
    loop {
        let line = receiver.lock().unwrap().recv().unwrap();

        if line.is_none() {
            break;
        }

        let line = line.unwrap();

        writeln!(stdin, "{}", line).unwrap();
    }
}

fn remote_output(stdout: std::process::ChildStdout) {
    let mut bufread = std::io::BufReader::new(stdout);
    let mut buf = String::new();

    while let Ok(n) = bufread.read_line(&mut buf) {
        if n > 0 {
            print!("{}", buf);
            buf.clear();
        } else {
            break;
        }
    }
}

fn parse_address(input: &str) -> Option<(String, u16, u16)> {
    let parts: Vec<&str> = input.split([':', '/'].as_ref()).collect();

    if parts.len() == 3 {
        if let Ok(port) = parts[1].parse::<u16>() {
            if let Ok(number) = parts[2].parse::<u16>() {
                return Some((parts[0].to_string(), port, number));
            }
        }
    }

    None
}

mod local;
mod remote;

use crate::local::run_local;
use crate::remote::start_remote;
use clap::Parser;

#[derive(Parser)]
struct Args {
    /// Number of threads
    #[clap(
        short = 'J',
        long = "parallel",
        value_name = "THREADS",
        conflicts_with = "remote",
        allow_negative_numbers = false,
        required = true
    )]
    num_threads: Option<i32>,

    /// Number of remotes
    #[clap(
        short = 'r',
        long = "remote",
        value_name = "REMOTES",
        conflicts_with = "num_threads",
        required = true
    )]
    remote: Option<Vec<String>>,

    /// Decide method of termination
    #[clap(short = 'e', long = "halt", value_name = "HALT", default_value = "never", value_parser = ["never", "lazy", "eager"])]
    termination: String,
}

fn main() -> std::io::Result<()> {
    let args = Args::parse();

    let termination = args.termination;

    if let Some(num_threads) = args.num_threads {
        run_local(num_threads, &termination);
    } else if let Some(remote_string) = args.remote {
        start_remote(&remote_string, &termination);
    }

    Ok(())
}

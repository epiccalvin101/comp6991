#!/bin/bash

# Check if an argument is provided
if [ $# -ne 2 ]; then
  echo "Usage: $0 <workshop|lab> <week no.>"
  exit 1
fi

# Get the URL from the command-line argument
URL="https://dvorak.cse.unsw.edu.au/~cs6991/23T3/$1/$2/starter.tar"

# Extract the filename from the URL
OUTPUT_FILE=$(basename "$URL")

# Download the tar file
echo "Downloading $OUTPUT_FILE..."
curl -O "$URL"

# Check if the download was successful
if [ $? -eq 0 ]; then
  echo "Download successful."

  # Create a directory to unpack the tar file
  OUTPUT_DIR="$1$2"
  echo "Creating directory: $OUTPUT_DIR"
  mkdir -p "$OUTPUT_DIR"

  # Unpack the tar file
  echo "Unpacking $OUTPUT_FILE into $OUTPUT_DIR..."
  tar -xf "$OUTPUT_FILE" -C "$OUTPUT_DIR/"

  # Check if the unpacking was successful
  if [ $? -eq 0 ]; then
    echo "Unpacking successful."
  else
    echo "Error: Unable to unpack $OUTPUT_FILE."
    exit 1
  fi
else
  echo "Error: Unable to download $OUTPUT_FILE."
  exit 1
fi

# Cleanup: Remove the downloaded tar file
echo "Cleaning up..."
rm "$OUTPUT_FILE"

echo "Done."
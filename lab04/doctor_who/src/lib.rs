//! doctor_who
//! Crate for caesar shifting strings

/// If no shift is specified, provides a default value
const DEFAULT_SHIFT: i32 = 5;
/// Value of the ASCII 'A'
const UPPERCASE_A: i32 = 65;
/// VAlue of the ASCII 'a'
const LOWERCASE_A: i32 = 97;
/// The number of letters in the alphabet
const ALPHABET_SIZE: i32 = 26;

/// Shift each String in a Vec by the given value 
///
/// ```
/// # use doctor_who::caesar_shift;
/// // Shift each line by the default shift (5)
/// assert_eq!(caesar_shift(None, vec![
///     String::from("Hello, World!")
/// ]), vec![String::from("Mjqqt, Btwqi!")]);
/// ```
/// 
/// ```
/// # use doctor_who::caesar_shift;
/// assert_eq!(caesar_shift(Some(3), vec![
///     String::from("Another example")
/// ]), vec![String::from("Dqrwkhu hadpsoh")]);
/// ```
pub fn caesar_shift(shift_by: Option<i32>, lines: Vec<String>) -> Vec<String> {
    let shift_number = shift_by.unwrap_or(DEFAULT_SHIFT);
    
    // no idea what this is doing? Ask the forums and/or 
    // look back at the functional programming lectures!
    lines
        .iter()
        .map(|line| shift(shift_number, line.to_string()))
        .collect()
}

fn shift(shift_by: i32, line: String) -> String {
    let mut result: Vec<char> = Vec::new();

    // turn shift_by into a positive number between 0 and 25
    let shift_by = shift_by % ALPHABET_SIZE + ALPHABET_SIZE;

    line.chars().for_each(|c| {
        let ascii = c as i32;

        if ('A'..='Z').contains(&c) {
            result.push(to_ascii(
                abs_modulo((ascii - UPPERCASE_A) + shift_by, ALPHABET_SIZE) + UPPERCASE_A,
            ));
        } else if ('a'..='z').contains(&c) {
            result.push(to_ascii(
                abs_modulo((ascii - LOWERCASE_A) + shift_by, ALPHABET_SIZE) + LOWERCASE_A,
            ));
        } else {
            result.push(c)
        }
    });

    result.iter().collect()
}

fn abs_modulo(a: i32, b: i32) -> i32 {
    (a % b).abs()
}

fn to_ascii(i: i32) -> char {
    char::from_u32(i as u32).unwrap()
}
